import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ExcelUploadComponent } from './excel-upload/excel-upload.component';
import { TruckLoadSheetComponent } from './truck-load-sheet/truck-load-sheet.component';
import { VendorDetailsComponent } from './vendor-details/vendor-details.component';
import { GrnComponent } from './grn/grn.component';

@NgModule({
  declarations: [
    AppComponent,
    ExcelUploadComponent,
    TruckLoadSheetComponent,
    VendorDetailsComponent,
    GrnComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
