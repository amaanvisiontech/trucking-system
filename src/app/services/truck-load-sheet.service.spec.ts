import { TestBed } from '@angular/core/testing';

import { TruckLoadSheetService } from './truck-load-sheet.service';

describe('TruckLoadSheetService', () => {
  let service: TruckLoadSheetService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TruckLoadSheetService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
