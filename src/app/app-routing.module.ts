import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExcelUploadComponent } from './components/excel-upload/excel-upload.component';
import { TruckLoadSheetComponent } from './components/truck-load-sheet/truck-load-sheet.component';
import { VendorDetailsComponent } from './components/vendor-details/vendor-details.component';
import { GrnComponent } from './components/grn/grn.component';

const routes: Routes = [
  { path: 'excel-upload', component: ExcelUploadComponent },
  { path: 'truck-load-sheet', component: TruckLoadSheetComponent },
  { path: 'vendor-details', component: VendorDetailsComponent },
  { path: 'grn', component: GrnComponent },
  { path: '', redirectTo: '/excel-upload', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
