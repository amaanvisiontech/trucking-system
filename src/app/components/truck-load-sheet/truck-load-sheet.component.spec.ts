import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TruckLoadSheetComponent } from './truck-load-sheet.component';

describe('TruckLoadSheetComponent', () => {
  let component: TruckLoadSheetComponent;
  let fixture: ComponentFixture<TruckLoadSheetComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TruckLoadSheetComponent]
    });
    fixture = TestBed.createComponent(TruckLoadSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
